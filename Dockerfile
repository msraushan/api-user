FROM node:12-alpine

RUN apk update && apk upgrade && apk add --no-cache --virtual .gyp python make g++ npm git

COPY package*.json ./
RUN npm install -g typescript ts-node
RUN npm install
COPY . app
WORKDIR app
RUN tsc -p . 
EXPOSE 8080 3000
CMD ["npm","start"]