import express from "express"
const bodyparser = require('body-parser')
const app = express()
import {User} from '../model/user'
app.use(express.json())
app.post('/users', async (req, res) => {
    console.log(req.body)
    const user = new User(req.body)
    console.log(user)
    try{
    await user.save()
    res.status(200).send(user)
    }
    catch(e){
        res.status(400).send(e)
    }
})
app.get('/users', async (req, res) => {
    const user = await User.find({})
    try{
        res.send(user)
    }
    catch(e){ res.status(500)}
})

export {app}