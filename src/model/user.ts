import validator from "validator"
import mongoose, { Schema } from "mongoose"
require('../db/mongoose')


const userSchema =new mongoose.Schema({
       name: {
           type: String,
           required:true,
           trim: true,
           lowercase:true
       },
       age: {
           type:Number,
           required:true,
           default: 0,
           validate(value:number): any{
               if(value < 0)
                    throw new Error("Enter the value greater than Zero")
           }
       },
       email: {
           type:String,
           validate(value:string):any{
           if(!validator.isEmail(value))
               throw new Error("Invalid Email Address")
           }
       },
}) as any;
const User = mongoose.model('User', userSchema)
export {User} 