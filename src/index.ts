import config from "../config"
require('./db/mongoose')
import {app} from './router/user'
const port = process.env.PORT || config.local
app.listen(port,()=> { console.log('Server up and running ...')})